# OpenQuizzes


## Crawl from http://jls.vnjpclub.com/

## Quiz structure

![Quiz strcuture](struct.png)

## Links

- http://jls.vnjpclub.com/trac-nghiem-han-tu-n5.html
- http://jls.vnjpclub.com/trac-nghiem-ngu-phap-n5.html
- http://jls.vnjpclub.com/trac-nghiem-dien-tu-n5.html
- http://jls.vnjpclub.com/trac-nghiem-han-tu-n4.html
- http://jls.vnjpclub.com/trac-nghiem-ngu-phap-n4.html
- http://jls.vnjpclub.com/trac-nghiem-dien-tu-n4.html
- http://jls.vnjpclub.com/trac-nghiem-chon-cau-n4.html
- http://jls.vnjpclub.com/trac-nghiem-han-tu-n3.html
- http://jls.vnjpclub.com/trac-nghiem-han-tu-n2.html
- http://jls.vnjpclub.com/trac-nghiem-han-tu-n1.html

## Commands


`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-han-tu-n5.html -k "Tiếng Nhật;N5;Hán tự" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-ngu-phap-n5.html -k "Tiếng Nhật;N5;Ngữ pháp" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-dien-tu-n5.html -k "Tiếng Nhật;N5;Điền từ" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-han-tu-n4.html -k "Tiếng Nhật;N4;Hán tự" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-ngu-phap-n4.html -k "Tiếng Nhật;N4;Ngữ pháp" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-dien-tu-n4.html -k "Tiếng Nhật;N4;Điền từ" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-chon-cau-n4.html -k "Tiếng Nhật;N4;Chọn câu" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-han-tu-n3.html -k "Tiếng Nhật;N3;Hán tự" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-han-tu-n2.html -k "Tiếng Nhật;N2;Hán tự" -d ~/Downloads/jls.vnjpclub.com`

`./crawler vnjpclub:jls:simple -u http://jls.vnjpclub.com/trac-nghiem-han-tu-n1.html -k "Tiếng Nhật;N1;Hán tự" -d ~/Downloads/jls.vnjpclub.com`